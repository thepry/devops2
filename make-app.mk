USER = "$(shell id -u):$(shell id -g)"

app-build:
	docker-compose build

app:
	docker-compose up

app-down:
	docker-compose down

app-bash:
	docker-compose run --rm app bash

app-install:
	docker-compose run --rm app bundle

app-db-drop:
	docker-compose run --rm app rails db:drop

app-db-prepare:
	docker-compose run --rm app rails db:create
	docker-compose run --rm app rails db:migrate
