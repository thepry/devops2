provider "aws" {
	version = "~> 1.5"
	region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "epta-infrastructure"
    key    = "terraform"
    region = "us-east-2"
  }
}

