resource "aws_route53_zone" "epta-site" {
  name = "epta.site."
}

resource "aws_route53_record" "epta-site-ns" {
  zone_id = "${aws_route53_zone.epta-site.zone_id}"
  name    = "epta.site"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.epta-site.name_servers.0}",
    "${aws_route53_zone.epta-site.name_servers.1}",
    "${aws_route53_zone.epta-site.name_servers.2}",
    "${aws_route53_zone.epta-site.name_servers.3}",
  ]
}

resource "aws_route53_record" "epta-site-a" {
  zone_id = "${aws_route53_zone.epta-site.zone_id}"
  name    = "epta.site."
  type    = "A"

  alias {
    name                   = "${aws_lb.epta.dns_name}"
    zone_id                = "${aws_lb.epta.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "epta-site-b" {
  zone_id = "${aws_route53_zone.epta-site.zone_id}"
  name    = "www"
  type    = "A"

  alias {
    name                   = "${aws_lb.epta.dns_name}"
    zone_id                = "${aws_lb.epta.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "epta-site-cdn" {
  zone_id = "${aws_route53_zone.epta-site.zone_id}"
  name    = "cdn"
  type    = "CNAME"
  ttl     = "30"
  records = ["dn973t8s80dq0.cloudfront.net"]

}
