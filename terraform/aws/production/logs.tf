resource "aws_cloudwatch_log_group" "eptalogs" {
  name = "eptalogs"

  tags {
    Environment = "production"
    Application = "epta"
  }
}

resource "aws_cloudwatch_log_stream" "eptalogstream" {
  name           = "eptalogstream"
  log_group_name = "${aws_cloudwatch_log_group.eptalogs.name}"
}
