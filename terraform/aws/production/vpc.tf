resource "aws_vpc" "epta" {
	cidr_block = "172.16.0.0/24"
	# instance_tenancy = "dedicated"

	tags {
		Name = "epta"
    Project = "epta"
	}
}

resource "aws_internet_gateway" "epta" {
  vpc_id = "${aws_vpc.epta.id}"

  tags {
		Name = "epta"
    Project = "epta"
  }
}

resource "aws_subnet" "epta-a" {
	vpc_id = "${aws_vpc.epta.id}"
	cidr_block = "172.16.0.0/27"
	availability_zone = "us-east-2a"

	tags {
		Name = "epta"
    Project = "epta"
	}
}

resource "aws_subnet" "epta-b" {
	vpc_id = "${aws_vpc.epta.id}"
	cidr_block = "172.16.0.32/27"
	availability_zone = "us-east-2b"

	tags {
		Name = "epta"
    Project = "epta"
	}
}

resource "aws_subnet" "epta-db" {
	vpc_id = "${aws_vpc.epta.id}"
	cidr_block = "172.16.0.64/27"

	tags {
		Name = "epta-db"
    Project = "epta"
	}
}

resource "aws_security_group" "epta-ssh" {
  name        = "epta-ssh"
  description = "allow ssh"
  vpc_id      = "${aws_vpc.epta.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

	tags {
		Name = "epta-ssh"
    Project = "epta"
	}
}

resource "aws_security_group" "epta-http" {
  name        = "epta-http"
  description = "allow http"
  vpc_id      = "${aws_vpc.epta.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

	tags {
		Name = "epta-http"
    Project = "epta"
	}
}

resource "aws_main_route_table_association" "epta" {
  vpc_id = "${aws_vpc.epta.id}"
  route_table_id = "${aws_route_table.epta.id}"
}

resource "aws_route_table" "epta" {
  vpc_id = "${aws_vpc.epta.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.epta.id}"
  }

  tags {
    Name = "epta"
  }
}

resource "aws_security_group" "epta-db" {
  name        = "epta-db"
  description = "allow connect to db"
  vpc_id      = "${aws_vpc.epta.id}"

	tags {
		Name = "epta-db"
	}
}

resource "aws_security_group_rule" "allow-tcp" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  source_security_group_id = "${aws_security_group.epta-http.id}"
  security_group_id = "${aws_security_group.epta-db.id}"
}

resource "aws_security_group_rule" "allow-udp" {
  type            = "ingress"
  from_port       = 0
  to_port         = 65535
  protocol        = "udp"
  source_security_group_id = "${aws_security_group.epta-http.id}"
  security_group_id = "${aws_security_group.epta-db.id}"
}

resource "aws_security_group_rule" "allow-icmp" {
  type            = "ingress"
  from_port       = 0
  to_port         = 8
  protocol        = "icmp"
  source_security_group_id = "${aws_security_group.epta-http.id}"
  security_group_id = "${aws_security_group.epta-db.id}"
}

resource "aws_security_group_rule" "allow-tcp-db-port" {
  type            = "ingress"
  from_port       = 5432
  to_port         = 5432
  protocol        = "tcp"
  source_security_group_id = "${aws_security_group.epta-http.id}"
  security_group_id = "${aws_security_group.epta-db.id}"
}

