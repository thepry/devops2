resource "aws_instance" "epta-web" {
  ami           = "ami-2581aa40"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.epta-a.id}"
  associate_public_ip_address = true
  key_name = "${aws_key_pair.eugene.id}"
  vpc_security_group_ids = [
    "${aws_security_group.epta-ssh.id}",
    "${aws_security_group.epta-http.id}"
  ]

  iam_instance_profile = "${aws_iam_instance_profile.epta_logger_profile.name}"

  tags {
    Name = "epta-web"
    Project = "epta"
  }
}

resource "aws_lb" "epta" {
  name = "epta"
  internal = false

  security_groups = ["${aws_security_group.epta-http.id}"]
  subnets = ["${aws_subnet.epta-a.id}", "${aws_subnet.epta-b.id}"]
  tags {
    Name = "epta"
    Project = "epta"
  }
}

resource "aws_lb_target_group" "epta" {
  name     = "epta"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.epta.id}"
}

resource "aws_lb_target_group_attachment" "epta" {
  target_group_arn = "${aws_lb_target_group.epta.arn}"
  target_id        = "${aws_instance.epta-web.id}"
  port             = 80
}

resource "aws_lb_listener" "epta" {
  load_balancer_arn = "${aws_lb.epta.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.epta.arn}"
    type             = "forward"
  }
}

