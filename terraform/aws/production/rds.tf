resource "aws_db_subnet_group" "epta" {
  name       = "main"
  subnet_ids = ["${aws_subnet.epta-a.id}", "${aws_subnet.epta-b.id}"]

  tags {
    Name = "epta"
    Project = "epta"
  }
}

resource "aws_db_instance" "epta" {
  name = "epta-database"
  allocated_storage    = 10
  engine               = "postgres"
  availability_zone    = "us-east-2a"
  instance_class       = "db.t2.micro"
  name                 = "${var.db_name}"
  username             = "${var.db_username}"
  password             = "${var.db_password}"
  db_subnet_group_name = "${aws_db_subnet_group.epta.id}"
  vpc_security_group_ids = [
    "${aws_security_group.epta-db.id}"
  ]
}

