docker-push-latest:
	docker tag devopshexlet_app:latest thepry/devopshexlet_app
	docker push thepry/devopshexlet_app

docker-build-prod:
	docker build -f services/app/Dockerfile.production -t 'devopshexlet_app:latest' services/app/.

docker-nginx-build:
	docker build -f services/nginx/Dockerfile -t 'devops-nginx:latest' services/nginx/.

docker-nginx-push:
	docker tag devops-nginx:latest thepry/devops-nginx
	docker push thepry/devops-nginx
