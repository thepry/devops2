ansible-vaults-encrypt:
	ansible-vault encrypt ansible/development/group_vars/all/vault.yml

ansible-vaults-decrypt:
	ansible-vault decrypt ansible/development/group_vars/all/vault.yml

ansible-vaults-edit-development:
	EDITOR=nvim ansible-vault edit ansible/development/group_vars/all/vault.yml

ansible-vaults-edit-production:
	EDITOR=nvim ansible-vault edit ansible/production/group_vars/all/vault.yml

ansible-vaults-view-production:
	ansible-vault view ansible/production/group_vars/all/vault.yml

ansible-deps-install:
	ansible-galaxy install -r requirements.yml

