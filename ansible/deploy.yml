---
- hosts: all
  gather_facts: no
  vars_prompt:
    - name: "app_image_tag"
      prompt: "App image tag"
      default: "latest"
      private: no

    - name: "nginx_image_tag"
      prompt: "Nginx image tag"
      default: "latest"
      private: no

  tasks:
    - set_fact: app_image_tag={{ app_image_tag }}
      tags: always

    - set_fact: nginx_image_tag={{ nginx_image_tag }}
      tags: always

- hosts: webservers
  gather_facts: no

  tasks:
    - name: notify slack
      local_action:
        module: slack
        domain: eptadevops.slack.com
        token: "{{ vault_app_slack_token }}"
        msg: "Epta deploy started: {{ app_image_name }}:{{ app_image_tag }}"
        channel: "operation"
        username: "{{ ansible_ssh_user }}"
      run_once: yes

    - name: Create a network
      docker_network:
        name: appnetwork
      tags: [network]

    - name: download nginx image
      docker_image:
        name: "{{ nginx_image_name }}:{{ nginx_image_tag }}"
        force: yes
      tags: [nginx]

    - name: start nginx container
      docker_container:
        recreate: yes
        name: nginx
        image: "{{ nginx_image_name }}:{{ nginx_image_tag }}"
        state: started
        restart_policy: always
        ports:
          - "80:80"
        networks:
          - name: appnetwork
      tags: [nginx]

    - name: download image
      docker_image:
        name: "{{ app_image_name }}:{{ app_image_tag }}"
        force: yes

    - name: run migrations
      docker_container:
        recreate: yes
        detach: no
        name: workshop-migrations
        command: "bin/rails db:migrate"
        image: "{{ app_image_name }}:{{ app_image_tag }}"
        state: started
        # log_driver: awslogs
        # log_options:
        #   awslogs-group: eptatestloggroup
        #   awslogs-stream: eptalogstream
        #   awslogs-region: '{{ aws_region }}'
        env_file: "{{ app_env_file }}"
        env:
          RAILS_ENV: production
      run_once: yes
      tags: [webserver]

    - name: start application
      docker_container:
        recreate: yes
        name: app-web
        image: "{{ app_image_name }}:{{ app_image_tag }}"
        command: "bin/rails server -e production"
        state: started
        log_driver: awslogs
        log_options:
          awslogs-group: eptalogs
          awslogs-stream: eptalogstream
          awslogs-region: '{{ aws_region }}'
        restart_policy: always
        env_file: "{{ app_env_file }}"
        env:
          RAILS_ENV: production
        ports:
          - "{{ app_port }}:{{ app_port }}"
        networks:
          - name: appnetwork
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock"
          - "/tmp:/tmp"
          - "/opt:/opt"
          - "/var/tmp:/var/tmp"
          - "/var/tmp:/var/tmp"
      tags: [webserver]

    - name: notify slack
      local_action:
        module: slack
        domain: eptadevops.slack.com
        token: "{{ vault_app_slack_token }}"
        msg: "Epta deploy completed: {{ app_image_name }}:{{ app_image_tag }}"
        channel: "#operation"
        username: "{{ ansible_ssh_user }}"
      run_once: yes
